package com.example.demo.event;

import com.example.demo.entity.Status;

public class AccountActivatedEvent extends BaseEvent<String> {
	public final Status status;

	public AccountActivatedEvent(String id, Status status) {
		super(id);
		this.status = status;
	}
}

package com.example.demo.services;

import java.util.List;

import com.example.demo.entity.AccountQueryEntity;

public interface AccountQueryService {
    public List<Object> listEventsForAccount(String accountNumber);

    public AccountQueryEntity getAccount(String accountNumber);

}

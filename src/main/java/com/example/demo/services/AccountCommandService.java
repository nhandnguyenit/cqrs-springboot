package com.example.demo.services;

import java.util.concurrent.CompletableFuture;

import com.example.demo.entity.AccountCreateDTO;
import com.example.demo.entity.MoneyCreditDTO;
import com.example.demo.entity.MoneyDebitDTO;

public interface AccountCommandService {

	public CompletableFuture<String> createAccount(AccountCreateDTO accountCreateDTO);

	public CompletableFuture<String> creditMoneyToAccount(String accountNumber, MoneyCreditDTO moneyCreditDTO);

	public CompletableFuture<String> debitMoneyFromAccount(String accountNumber, MoneyDebitDTO moneyDebitDTO);
}
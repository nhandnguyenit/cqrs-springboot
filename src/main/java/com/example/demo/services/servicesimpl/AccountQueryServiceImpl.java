package com.example.demo.services.servicesimpl;

import java.util.List;
import java.util.stream.Collectors;

import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.stereotype.Service;

import com.example.demo.entity.AccountQueryEntity;
import com.example.demo.repository.AccountRepository;
import com.example.demo.services.AccountQueryService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AccountQueryServiceImpl implements AccountQueryService {

    private final EventStore eventStore;
    private final AccountRepository accountRepository;

    @Override
    public List<Object> listEventsForAccount(String accountNumber) {
        return eventStore.readEvents(accountNumber).asStream().map(s -> s.getPayload()).collect(Collectors.toList());
    }

    @Override
    public AccountQueryEntity getAccount(String accountNumber) {
        return accountRepository.findById(accountNumber).get();
    }

}
